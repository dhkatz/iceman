#ifndef ACTOR_H_
#define ACTOR_H_

#include <memory>
#include <stack>
#include "GraphObject.h"

class StudentWorld;

/*************************************//**
*   Actor CLASS
*****************************************/

class Actor : public GraphObject {
public:
    enum ENT_CLASS {
        ENT_SOLID, ENT_PLAYER, ENT_NPC, ENT_ITEM, ENT_WORLD, ENT_PROJECTILE
    };

    Actor(int image_id, int start_x, int start_y, Direction dir, double size, size_t depth, bool is_alive = true,
          StudentWorld* world = nullptr);

    virtual ENT_CLASS getClass() = 0;

    StudentWorld* getWorld();

    bool isAlive();

    void moveDir(GraphObject::Direction direction, size_t distance);

    virtual void annoy(size_t health, ENT_CLASS attacker) = 0;

    virtual void die();

    virtual void doSomething() = 0;

private:
    StudentWorld* world_;
    bool is_alive_;
};

/*************************************//**
*   ICE CLASS
*****************************************/

class Ice : public Actor {
public:
    explicit Ice(int start_x = 0, int start_y = 0, int image_id = 6, Direction dir = right, double size = 0.25,
                 size_t depth = 3);

    ~Ice() override = default;

    ENT_CLASS getClass() override;

    void annoy(size_t health, ENT_CLASS attacker) override {};

    void doSomething() override {};
};

/*************************************//**
*   SQUIRT CLASS
*****************************************/

class Squirt : public Actor {
public:
    Squirt(int start_x, int start_y, Direction dir, StudentWorld* world);

    ~Squirt() override = default;

    ENT_CLASS getClass() override;

    void annoy(size_t health, ENT_CLASS attacker) override {};

    void doSomething() override;

private:
    size_t distance_;
};

/*************************************//**
*   BOULDER CLASS
*****************************************/

class Boulder : public Actor {
public:
    enum STATE {
        STABLE, WAIT, FALL
    };

    Boulder(int start_x, int start_y, StudentWorld* world);

    ENT_CLASS getClass() override;

    STATE getState();

    size_t getWait();

    void setState(STATE state);

    void setWait(size_t time);

    void annoy(size_t health, ENT_CLASS attacker) override {};

    void doSomething() override;

private:
    STATE state_;
    size_t wait_time_;
};

/*************************************//**
*   ITEM CLASS
*****************************************/

class Item : public Actor {
public:
    Item(int image_id, int x, int y, Direction dir, double size, size_t depth, StudentWorld* world);

    ~Item() override = default;

    ENT_CLASS getClass() override;

    void annoy(size_t health, ENT_CLASS attacker) override {};

    void doSomething() override = 0;
};

/*************************************//**
*   OIL CLASS
*****************************************/

class Oil : public Item {
public:
    Oil(int x, int y, StudentWorld* world);

    void doSomething() override;
};

/*************************************//**
*   GOLD CLASS
*****************************************/

class Gold : public Item {
public:
    enum STATE {
        PERM, TEMP
    };

    Gold(int x, int y, StudentWorld* world);

    Gold(int x, int y, StudentWorld* world, STATE state);

    STATE getState();

    void setState(STATE state);

    void doSomething() override;

private:
    STATE state_;
    size_t lifetime_;
};

/*************************************//**
*   SONAR CLASS
*****************************************/

class Sonar : public Item {
public:
    enum STATE {
        PERM, TEMP
    };

    Sonar(int x, int y, StudentWorld* world);

    STATE getState();

    void setState(STATE state);

    void doSomething() override;

private:
    STATE state_;
    size_t lifetime_;
};

/*************************************//**
*   WATER CLASS
*****************************************/

class Water : public Item {
public:
    enum STATE {
        PERM, TEMP
    };

    Water(int x, int y, StudentWorld* world);

    STATE getState();

    void setState(STATE state);

    void doSomething() override;

private:
    STATE state_;
    size_t lifetime_;
};

/*************************************//**
*   CHARACTER CLASS
*****************************************/

class Character : public Actor {
public:
    Character(int image_id, int start_x, int start_y, Direction dir, size_t health,
              StudentWorld* world);

    ~Character() override = default;

    ENT_CLASS getClass() override = 0;

    size_t getHealth() const;

    void setHealth(size_t health);

    void annoy(size_t health, ENT_CLASS attacker) override = 0;

    void hurt(size_t damage);

    void die() override;

private:
    size_t health_;
};

/*************************************//**
*   PROTESTER CLASS
*****************************************/

class Protester : public Character {
public:
    enum STATE {
        LEAVE, REST, NORMAL
    };

    explicit Protester(StudentWorld* world, int image_id = IID_PROTESTER, size_t health = 5);

    ENT_CLASS getClass() override;

    STATE getState();

    STATE getLastState();

    virtual size_t getScore();

    std::stack<std::pair<int, int>> & getPath();

    void setState(STATE state);

    void setLastState(STATE state);

    void setPath(const std::stack<std::pair<int, int>> & path);

    void setMove(size_t amount);

    void setTime(size_t time);

    void rest();

    void leave();

    virtual void action();

    bool chase();

    bool shout();

    void resetOrTurn();

    virtual void bribed();

    void annoy(size_t health, ENT_CLASS attacker) override;

    void die() override;

    void doSomething() override;

private:
    STATE state_;
    STATE last_state_;
    std::stack<std::pair<int, int>> path_;
    size_t amount_to_move_;
    size_t tick_;
    size_t shout_time_;
    size_t turn_time_;
};

/*************************************//**
*   HARDCORE PROTESTER CLASS
*****************************************/

class HardcoreProtester : public Protester {
public:
    explicit HardcoreProtester(StudentWorld* world, int image_id = IID_HARD_CORE_PROTESTER, size_t health = 20);

    void action() override;

    void bribed() override;

    bool cellphone();

    size_t getScore() override;

private:
};

/*************************************//**
*   ICEMAN CLASS
*****************************************/

class Iceman : public Character {
public:
    explicit Iceman(StudentWorld* world, int image_id = IID_PLAYER, int start_x = 30, int start_y = 60,
                    Direction dir = right, size_t health = 10);

    ENT_CLASS getClass() override;

    size_t getWater() const;

    size_t getSonar() const;

    size_t getGold() const;

    void setWater(size_t water);

    void setSonar(size_t sonar);

    void setGold(size_t gold);

    void annoy(size_t health, ENT_CLASS attacker) override;

    void die() override;

    void doSomething() override;

    void squirt();

    void sonar();

    void drop();

private:
    size_t water_;
    size_t sonar_;
    size_t gold_;
};

/*************************************//**
*   AREA CLASS
*****************************************/

struct Area {
    Area(std::pair<int, int> pair, GraphObject::Direction dir, size_t size);

    std::pair<int, int> tail;
    std::pair<int, int> head;
    GraphObject::Direction dir;
    size_t size;
};

#endif // ACTOR_H_
