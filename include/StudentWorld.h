#ifndef STUDENTWORLD_H_
#define STUDENTWORLD_H_

//#define ICEMAN_DEBUG

#include <string>
#include <utility>
#include <vector>
#include <algorithm>
#include <initializer_list>
#include <stack>
#include <cfloat>
#include <queue>

#include "GameConstants.h"
#include "GameWorld.h"
#include "Actor.h"

const size_t ROW = 60;
const size_t COL = 64;

class StudentWorld : public GameWorld {
public:
    explicit StudentWorld(std::string assetDir) : GameWorld(std::move(assetDir)) {
        ice_field_ = {};
#ifdef ICEMAN_DEBUG
        std::cout << "Creating log file\n";
        debug_log_.open("iceman_log.txt", std::ofstream::out);
#endif
    }

    ~StudentWorld() override = default;

    void debug(std::string data);

    int init() override;

    int move() override;

    void cleanUp() override;

    void updateText();

    std::shared_ptr<Actor> inRadius(int x, int y, size_t r, std::initializer_list<Actor::ENT_CLASS> filter);

    GraphObject::Direction getDir(std::pair<int, int> start, std::pair<int, int> end);

    std::pair<bool, std::stack<std::pair<int, int>>>
    getPath(std::pair<int, int> start, std::pair<int, int> goal, size_t size);

    std::stack<std::pair<int, int>>
    getSteps(const std::map<std::pair<int, int>, std::pair<int, int>> &came_from, std::pair<int, int> current);

    std::shared_ptr<Iceman> getIceman();

    bool isSolid(int x, int y, size_t r);

    bool isClear(Area area, std::initializer_list<Actor::ENT_CLASS> filter = {Actor::ENT_WORLD});

    bool isClear(std::pair<int, int> pos, std::initializer_list<Actor::ENT_CLASS> filter = {Actor::ENT_WORLD});

    bool removeIce(int x, int y);

    void removeOil();

    void revealObjects(std::pair<int, int> pos, size_t radius);

    void spawnProtester();

    void spawnGoodie();

    template<typename T>
    void spawnObjects(size_t num_objects, bool take_space = false, size_t range = 0, size_t domain = 0);

    void spawnObject(std::shared_ptr<Actor> object);

private:
#ifdef ICEMAN_DEBUG
    std::ofstream debug_log_{};
#endif
    std::vector<std::vector<std::shared_ptr<Ice>>> ice_field_;
    std::vector<std::shared_ptr<Actor>> actors_;
    std::shared_ptr<Iceman> iceman_;

    size_t num_oil_{};
    size_t num_boulder_{};
    size_t num_gold_{};
    size_t num_protester_{};

    int last_protester_{};

    std::set<std::pair<int, int>> closed_set{};
    std::set<std::pair<int, int>> open_set{};
    std::map<std::pair<int, int>, std::pair<int, int>> came_from{};
    std::map<std::pair<int, int>, double> g_score{};
    std::map<std::pair<int, int>, double> f_score{};

    std::map<GraphObject::Direction, std::pair<int, int>> dirs = {
            {GraphObject::Direction::up,    {0,    1}},
            {GraphObject::Direction::down,  {0,    -1}},
            {GraphObject::Direction::left,  {-1,   0}},
            {GraphObject::Direction::right, {1, 0}},
    };
};

#endif // STUDENTWORLD_H_
