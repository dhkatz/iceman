# Iceman (CLion version) #

In Iceman, the player starts out a new game with three lives and continues to play until
all of his/her lives have been exhausted. There are multiple levels in Iceman, beginning
with level 0, and each level has its own unique oil field configuration. During each level,
the Iceman (controlled by the player) must dig and find all of the oil hidden within the oil
field in order to move on to the next level.

### What is this repository for? ###

* Clion compatible skeleton for Iceman

### How do I get set up? ###

* Install [MinGW](https://sourceforge.net/projects/mingw/files/latest/download?source=files)
* Install the following packages inside MinGW
    * mingw-developer-toolkit
    * mingw32-base
    * mingw32-gcc-g++
    * msys-base
* Install [CLion](https://www.jetbrains.com/clion/) (free for students)
* Clone repo using Git (CLion has built in Git support, see their website)
* Download the [FreeGLUT MinGW package](http://files.transmissionzero.co.uk/software/development/GLUT/freeglut-MinGW.zip)
    * Place the freeglut.dll file from inside the bin folder into the bin/ directory where your .exe is compiled to
* Download [irrKlang 1.5.0](https://www.ambiera.com/irrklang/downloads.html)
    * Place the irrKlang.dll and ikpflac.dll files from inside the bin/win32-gcc into the bin/ directory where your .exe is compiled to

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
