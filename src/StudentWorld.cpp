#include <sstream>
#include <random>
#include <cfloat>

#include "StudentWorld.h"

GameWorld* createStudentWorld(std::string assetDir) {
    return new StudentWorld(std::move(assetDir));
}

/**
 * Setup the current level of the game.
 *
 * @return The state of the game after initialization
 */
int StudentWorld::init() {
    ice_field_ = std::vector<std::vector<std::shared_ptr<Ice>>>(COL, std::vector<std::shared_ptr<Ice>>(COL));
    for (int x = 0; x < COL; x++) {
        for (int y = 0; y < ROW; y++) {
            if (!(x >= 30 && x <= 33 && y >= 4 && y <= 59)) {
                ice_field_[x][y] = std::make_shared<Ice>(x, y);
            }
        }
    }

    iceman_ = std::make_shared<Iceman>(this);

    num_oil_ = std::min(2 + getLevel(), (size_t) 21);
    num_boulder_ = std::min(getLevel() / 2 + 2, (size_t) 9);
    num_gold_ = std::max(5 - getLevel() / 2, (size_t) 2);

    spawnObjects<Boulder>(num_boulder_, true, 20);
    spawnObjects<Oil>(num_oil_, false);
    spawnObjects<Gold>(num_gold_, false);

    return GWSTATUS_CONTINUE_GAME;
}

/**
 * Game's main tick processor (runs approximately 20 t/s)
 *
 * @return The status of the game during/after the current tick
 */
int StudentWorld::move() {
    updateText();

    spawnProtester();
    spawnGoodie();

    for (auto &actor : actors_) {
        actor->doSomething();
    }

    auto remove = std::remove_if(actors_.begin(), actors_.end(), [&, this](auto &actor) {
        if (!actor->isAlive() && actor->getClass() == Actor::ENT_NPC) {
            num_protester_--;
        }

        return !actor->isAlive();
    });
    actors_.erase(remove, actors_.end());

    if (!iceman_->isAlive()) {
        decLives();
        return GWSTATUS_PLAYER_DIED;
    }

    iceman_->doSomething();
    if (removeIce(iceman_->getX(), iceman_->getY())) {
        playSound(SOUND_DIG);

    }

    if (num_oil_ == 0) {
        playSound(SOUND_FINISHED_LEVEL);
        return GWSTATUS_FINISHED_LEVEL;
    }


    return GWSTATUS_CONTINUE_GAME;
}

/**
 * Clean up the current level in preparation for the next initialization.
 */
void StudentWorld::cleanUp() {
    last_protester_ = 0;
    num_protester_ = 0;
    actors_.clear();
    ice_field_.clear();
    iceman_.reset();
}

/**
 * Parse the game's information text visible at the top of the window.
 */
void StudentWorld::updateText() {
    std::ostringstream status_text;

    // Lambda so I don't have to repeat code.
    auto parse_length = [](auto number) {
        return (number < 10) ? ' ' + std::to_string(number) : std::to_string(number);
    };

    status_text << "Lvl: ";
    int level = getLevel();
    status_text << parse_length((level > 99) ? 99 : level);

    status_text << " Lives: ";
    status_text << std::to_string(getLives());

    status_text << " Hlth: ";
    size_t health = iceman_->getHealth() * 10;
    status_text << parse_length(health) << '%';

    status_text << " Wtr: ";
    size_t water = iceman_->getWater();
    status_text << parse_length((water > 99) ? 99 : water);

    status_text << " Gld: ";
    size_t gold = iceman_->getGold();
    status_text << parse_length((gold > 99) ? 99 : gold);

    status_text << "  Oil Left: ";
    status_text << parse_length(num_oil_);

    status_text << " Sonar: ";
    size_t sonar = iceman_->getSonar();
    status_text << parse_length((sonar > 99) ? 99 : sonar);

    status_text << " Scr: ";
    int score = getScore();
    score = (score > 999999) ? 999999 : score;
    status_text << std::string(6 - std::to_string(score).length(), '0').append(std::to_string(score));

    setGameStatText(status_text.str());
}

/**
 * Spawn a protester based on a probability
 */
void StudentWorld::spawnProtester() {
    if (last_protester_ <= 0) {
        if (num_protester_ < std::min(15, (int) (2 + getLevel() * 1.5))) {
            if (std::min(90, (int) (getLevel() * 10 + 30)) >= rand() % 100) {
                spawnObject(std::make_shared<HardcoreProtester>(this));
            }
            else {
                spawnObject(std::make_shared<Protester>(this));
            }
            num_protester_++;
            last_protester_ = std::max(25, (int) (200 - getLevel()));
        }
    }
    else {
        last_protester_--;
    }
}

/**
 * Spawn a Goodie based on probability
 */
void StudentWorld::spawnGoodie() {
    auto chance = getLevel() * 25 + 300;
    if (std::rand() % chance + 1 == 1) {
        if (std::rand() % 5 + 1 != 1) {
            int x = std::rand() % 61, y = std::rand() % 61;
            while (true) {
                if (isClear(Area({x, y}, GraphObject::none, 0), {Actor::ENT_WORLD, Actor::ENT_SOLID}) && !inRadius(x, y, 6, {Actor::ENT_ITEM, Actor::ENT_SOLID})) {
                    break;
                }
                x = std::rand() % 61;
                y = std::rand() % 61;
            }

            spawnObject(std::make_shared<Water>(x, y, this));
        }
        else {
            spawnObject(std::make_shared<Sonar>(0, 60, this));
        }
    }
}

/**
 * Search for and retrieve a type of object within a radius of a position.
 *
 * @param x X-Coordinate of search circle
 * @param y Y-Coordinate of search circle
 * @param r Radius of search circle
 * @param filter Initializer list of ENT_IDs of objects to search for
 * @return std::shared_ptr<Actor> of found object or nullptr
 */
std::shared_ptr<Actor> StudentWorld::inRadius(int x, int y, size_t r, std::initializer_list<Actor::ENT_CLASS> filter) {
    for (auto item : filter) {
        switch (item) {
            case Actor::ENT_PLAYER: {
                if (std::sqrt(std::pow(iceman_->getX() - x, 2) + std::pow(iceman_->getY() - y, 2)) <= r) {
                    return iceman_;
                }
                break;
            }
            default:
                for (auto actor: actors_) {

                    if (actor->getClass() == item &&
                        std::sqrt(std::pow(actor->getX() - x, 2) + std::pow(actor->getY() - y, 2)) <= r) {
                        return actor;
                    }
                }
                break;
        }
    }

    return nullptr;
}

/**
 * Calculate the direction between to points
 *
 * @param start The starting point
 * @param end The ending point
 * @return GraphObject::Direction Direction between points
 */
GraphObject::Direction StudentWorld::getDir(std::pair<int, int> start, std::pair<int, int> end) {
    if (end.first > start.first) {
        return GraphObject::Direction::right;
    }
    else if (end.first < start.first) {
        return GraphObject::Direction::left;
    }
    else if (end.second > start.second) {
        return GraphObject::Direction::up;
    }
    else if (end.second < start.second) {
        return GraphObject::Direction::down;
    }

    return GraphObject::Direction::none;
}

/**
 * Calculate the shortest path using A*
 *
 * @param start Starting position
 * @param goal Ending position
 * @param size Size of the sprite trying to traverse
 * @return std::pair<bool, std::stack<std::pair<int, int>>> Bool if the path was found and a stack of steps if true
 */
std::pair<bool, std::stack<std::pair<int, int>>>
StudentWorld::getPath(std::pair<int, int> start, std::pair<int, int> goal, size_t size) {
    open_set.insert(start);

    for (int i = 0; i < COL; ++i) {
        for (int j = 0; j < COL; ++j) {
            g_score[{i, j}] = DBL_MAX;
            f_score[{i, j}] = DBL_MAX;
        }
    }

    g_score[start] = 0;
    f_score[start] = std::abs(start.first - goal.first) + std::abs(start.second - goal.second);

    while (!open_set.empty()) {
        auto current = *std::min_element(open_set.begin(), open_set.end(), [&, this](auto a, auto b) mutable {
            return f_score[a] < f_score[b];
        });

        if (current == goal) {
            return {true, getSteps(came_from, current)};
        }

        open_set.erase(current);
        closed_set.insert(current);

        debug("[A-STAR] CURRENT (" + std::to_string(current.first) + ", " + std::to_string(current.second) +
              ")\n");

        for (auto dir : dirs) {
            auto neighbor = std::make_pair(current.first + dir.second.first, current.second + dir.second.second);

            debug("[A-STAR] NEIGH (" + std::to_string(current.first) + ", " + std::to_string(current.second) +
                  ")\n");

            if (neighbor.first < 0 || neighbor.first > COL || neighbor.second < 0 || neighbor.second > COL) {
                debug("[A-STAR] (" + std::to_string(neighbor.first) + ", " + std::to_string(neighbor.second) +
                      ") is OOB\n");
                continue; // Skip directions that take us OOB
            }

            if (closed_set.count(neighbor) > 0) {
                continue;
            }

            std::pair<int, int> real_neighbor;
            real_neighbor = {
                    current.first + (dir.second.first != 0 ? (dir.second.first == -1 ? -1 : size) : 0),
                    current.second + (dir.second.second != 0 ? (dir.second.second == -1 ? -1 : size) : 0)
            };

            debug("[A-STAR] REAL NEIGH (" + std::to_string(real_neighbor.first) + ", " +
                  std::to_string(real_neighbor.second) +
                  ")\n");

            bool is_blocked = false;
            for (int i = 0; i < size; ++i) {
                std::pair<int, int> temp;
                if (dir.second.first == 1 || dir.second.first == -1) {
                    temp = {real_neighbor.first, real_neighbor.second + i};
                }
                else {
                    temp = {real_neighbor.first + i, real_neighbor.second};
                }
                debug("[A-STAR] TEMP (" + std::to_string(neighbor.first) + ", " + std::to_string(neighbor.second) +
                      ")\n");
                //Area vec(std::make_pair(temp.first, temp.second), dir.first, 0);
                if (!isClear({temp.first, temp.second}, {Actor::ENT_WORLD, Actor::ENT_SOLID})) {
                    debug("[A-STAR] TEMP (" + std::to_string(neighbor.first) + ", " + std::to_string(neighbor.second) +
                          ") is blocked\n");
                    is_blocked = true;
                    break;
                }
            }

            if (is_blocked) {
                debug("[A-STAR] (" + std::to_string(neighbor.first) + ", " + std::to_string(neighbor.second) +
                      ") is blocked\n");
                continue; // Skip directions that are blocked
            }

            if (open_set.count(neighbor) == 0) {
                open_set.insert(neighbor);
            }

            auto tentative_g_score = g_score[current] - 1.0;
            if (tentative_g_score > g_score[neighbor]) {
                continue; // Skip directions that are worse than the current
            }

            came_from[neighbor] = current;
            g_score[neighbor] = tentative_g_score;
            f_score[neighbor] =
                    tentative_g_score + std::abs(neighbor.first - goal.first) + std::abs(neighbor.second - goal.second);
        }
    }
    debug("[A-STAR] Failed to find the shortest path!\n");
    return {false, std::stack<std::pair<int, int>>()};
}

/**
 * Build the stack of steps
 *
 * @param came_from A map that maps a point to the point it came from
 * @param current The current point to map from
 * @return A stack of positions to get to the end point
 */
std::stack<std::pair<int, int>>
StudentWorld::getSteps(const std::map<std::pair<int, int>, std::pair<int, int>> &came_from,
                       std::pair<int, int> current) {
    std::stack<std::pair<int, int>> total_path;
    total_path.push(current);
    while (came_from.count(current) > 0) {
        current = came_from.at(current);
        total_path.push(current);
    }

    open_set.clear();
    closed_set.clear();
    this->came_from.clear();
    g_score.clear();
    f_score.clear();
    return total_path;
}

/**
 * Get the Iceman object
 *
 * @return std::shared_ptr<Iceman> The Iceman object
 */
std::shared_ptr<Iceman> StudentWorld::getIceman() {
    return iceman_;
}

/**
 * Check if a location contains an object with collision.
 *
 * @param x X-Coordinate of location
 * @param y Y-Coordinate of location
 * @param r Radius to search around the location
 * @return bool Is the location a collision object
 */
bool StudentWorld::isSolid(int x, int y, size_t r) {
    return inRadius(x, y, r, {Actor::ENT_SOLID}) != nullptr;
}

/**
 * Check if an Area is clear
 *
 * @param area Area to check
 * @param filter Filter of objects to check for
 * @return bool Is the Area clear of objects in the filter
 */
bool StudentWorld::isClear(Area area, std::initializer_list<Actor::ENT_CLASS> filter) {
    for (int _x = area.tail.first; _x < area.head.first; ++_x) {
        for (int _y = area.tail.second; _y < area.head.second; ++_y) {
            if (_x < 0 || _x >= COL || _y < 0 || _y >= COL) {
                return false;
            }

            for (auto &item: filter) {
                switch (item) {
                    case Actor::ENT_WORLD:
                        if (ice_field_[_x][_y]) {
                            return false;
                        }
                        break;
                    default:
                        for (std::shared_ptr<Actor> &actor: actors_) {
                            if (actor->getClass() == item && actor->getX() == _x && actor->getY() == _y) {
                                return false;
                            }
                        }
                }
            }
        }
    }

    return true;
}

/**
 * Check if a singly point is clear or not
 *
 * @param pos The point to check
 * @param filter Filter of classes to check for
 * @return bool If the point is clear or not
 */
bool StudentWorld::isClear(std::pair<int, int> pos, std::initializer_list<Actor::ENT_CLASS> filter) {
    if (pos.first < 0 || pos.first >= COL || pos.second < 0 || pos.second >= COL) {
        return false;
    }

    for (auto &item: filter) {
        switch (item) {
            case Actor::ENT_WORLD:
                if (ice_field_[pos.first][pos.second] != nullptr) {
                    return false;
                }
                break;
            default:
                for (std::shared_ptr<Actor> &actor: actors_) {
                    if (actor->getClass() == item && actor->getX() == pos.first && actor->getY() == pos.second) {
                        return false;
                    }
                }
        }
    }

    return true;
}

/**
 * Remove Ice from a location.
 *
 * @param x X-Coordinate of the location
 * @param y Y-Coordinate of the location
 * @return bool If the location was cleared of Ice objects
 */
bool StudentWorld::removeIce(int x, int y) {
    bool was_removed = false;
    for (int _x = x; _x < x + 4; ++_x) {
        for (int _y = y; _y < y + 4; ++_y) {
            if (_x < 0 || _x >= COL || _y < 0 || _y >= ROW) {
                continue;
            }
            if (ice_field_[_x][_y]) {
                ice_field_[_x][_y] = nullptr;
                was_removed = true;
            }
        }
    }

    return was_removed;
}

/**
 * Decrement the oil counter in the world.
 */
void StudentWorld::removeOil() {
    num_oil_--;
}

/**
 * Reveal all Actors in a radius around a point
 *
 * @param pos The point to start from
 * @param radius The radius in which Actors are revealed
 */
void StudentWorld::revealObjects(std::pair<int, int> pos, size_t radius) {
    for (const auto &actor: actors_) {
        if (std::sqrt(std::pow(pos.first - actor->getX(), 2) + std::pow(pos.second - actor->getY(), 2)) <= radius) {
            actor->setVisible(true);
        }
    }
}

/**
 * Generate objects in a random locations that are spread out.
 *
 * @tparam T Class of the object to spawn
 * @param num_objects How many objects to spawn
 * @param take_space Does the object take space (replace Ice)
 * @param range Modifier to the lower bound of the spawn range (y-axis)
 * @param domain Modifier to the lower bound of the spawn domain (x-axis)
 */
template<typename T>
void StudentWorld::spawnObjects(size_t num_objects, bool take_space, size_t range, size_t domain) {
    // NOTE: Apparently random_device is broken in MinGW on Windows so I can't use the random library...
/*      auto rand_int = [](int a, int b) -> int {
        std::random_device random_device;
        std::mt19937 generator(random_device());
        std::uniform_int_distribution<int> distribution(a, b);
        return distribution(generator);
    };*/

    for (int i = 0; i < num_objects; ++i) {
        int x, y;
        do {
            // See note above
            //x = rand_int(0, 60);
            //y = rand_int(20, 56);
            x = std::rand() % 60;
            y = std::rand() % 35;
            x += domain;
            y += range;
        } while (inRadius(x, y, 6, {Actor::ENT_ITEM, Actor::ENT_SOLID}) ||
                 (x >= 27 && x <= 33 && y >= 4 && y <= 59) || (x < 4 || x > 60) || (y < 4));

        debug(typeid(T).name() + std::string(" spawned at (") + std::to_string(x) + ", " + std::to_string(y) + ")\n");
        actors_.push_back(std::make_shared<T>(x, y, this));

        if (take_space) {
            removeIce(x, y);
        }
    }
}

/**
 * Add a spawned object to the actor table.
 *
 * @param object The object that was spawned
 */
void StudentWorld::spawnObject(std::shared_ptr<Actor> object) {
    actors_.push_back(object);
}

/**
 * Debug function, print to a log file
 *
 * @param data std::string of data to log
 */
void StudentWorld::debug(std::string data) {
#ifdef ICEMAN_DEBUG
    if (!debug_log_.is_open()) {
        debug_log_.open("iceman_log.txt", std::ofstream::out | std::ofstream::app);
    }
    debug_log_ << data;
    debug_log_.close();
#endif
}
