#include "Actor.h"

#include <utility>
#include "StudentWorld.h"

/*************************************//**
*   ACTOR CLASS
*****************************************/

Actor::Actor(int image_id, int start_x, int start_y, GraphObject::Direction dir, double size, size_t depth,
             bool is_alive, StudentWorld* world) : GraphObject(image_id, start_x, start_y, dir, size,
                                                               depth) {
    world_ = world;
    is_alive_ = is_alive;
}

void Actor::die() {
    is_alive_ = false;
    setVisible(false);
}

StudentWorld* Actor::getWorld() {
    return world_;
}

bool Actor::isAlive() {
    return is_alive_;
}

void Actor::moveDir(GraphObject::Direction direction, size_t distance) {
    switch (direction) {
        case GraphObject::up:
            moveTo(getX(), getY() + distance);
            break;
        case GraphObject::down:
            moveTo(getX(), getY() - distance);
            break;
        case GraphObject::left:
            moveTo(getX() - distance, getY());
            break;
        case GraphObject::right:
            moveTo(getX() + distance, getY());
            break;
        default:
            break;
    }
}

/*************************************//**
*   ICE CLASS
*****************************************/

Ice::Ice(int start_x, int start_y, int image_id, GraphObject::Direction dir, double size, size_t depth)
        : Actor(image_id, start_x, start_y, dir, size, depth) {
    setVisible(true);
}

Actor::ENT_CLASS Ice::getClass() {
    return ENT_WORLD;
}

/*************************************//**
*   SQUIRT CLASS
*****************************************/

Squirt::Squirt(int start_x, int start_y, Direction dir, StudentWorld* world)
        : Actor(IID_WATER_SPURT, start_x, start_y, dir, 1.0, 1, true, world) {
    setVisible(true);
    distance_ = 0;
}

Actor::ENT_CLASS Squirt::getClass() {
    return ENT_PROJECTILE;
}

void Squirt::doSomething() {
    std::shared_ptr<Actor> protester = getWorld()->inRadius(getX(), getY(), 3, {ENT_NPC});
    if (protester) {
        protester->annoy(2, ENT_PLAYER);
        die();
        return;
    }

    Direction dir = getDirection();
    if (++distance_ >= 4 ||
        !getWorld()->isClear(Area(std::make_pair(getX(), getY()), dir, 1), {ENT_WORLD, ENT_SOLID})) {
        die();
        return;
    }

    moveDir(dir, 1);
}

/*************************************//**
*   BOULDER CLASS
*****************************************/

Boulder::Boulder(int start_x, int start_y, StudentWorld* world)
        : Actor(IID_BOULDER, start_x, start_y, down, 1.0, 1, true, world) {
    setVisible(true);
    state_ = STABLE;
    wait_time_ = 0;
}

Boulder::STATE Boulder::getState() {
    return state_;
}

void Boulder::setState(STATE state) {
    state_ = state;
}

void Boulder::doSomething() {
    if (!isAlive()) {
        return;
    }

    switch (getState()) {
        case STABLE:
            if (getWorld()->isClear(Area(std::make_pair(getX(), getY()), down, 1))) {
                setState(WAIT);
                setWait(30);
            }
            break;
        case WAIT:
            if (getWait() == 0) {
                setState(FALL);
                getWorld()->playSound(SOUND_FALLING_ROCK);
                return;
            }

            setWait(getWait() - 1);
            break;
        case FALL:
            if (getY() - 1 >= 0 && getWorld()->isClear(Area(std::make_pair(getX(), getY()), down, 1))) {
                moveDir(down, 1);

                std::shared_ptr<Actor> hit = getWorld()->inRadius(getX(), getY(), 3, {ENT_NPC, ENT_PLAYER});
                if (hit) {
                    hit->annoy(20, ENT_SOLID);
                }
            }
            else {
                die();
            }
        default:
            break;
    }
}

void Boulder::setWait(size_t time) {
    wait_time_ = time;
}

size_t Boulder::getWait() {
    return wait_time_;
}

Actor::ENT_CLASS Boulder::getClass() {
    return ENT_SOLID;
}

/*************************************//**
*   ITEM CLASS
*****************************************/

Item::Item(int image_id, int x, int y, GraphObject::Direction dir, double size, size_t depth, StudentWorld* world)
        : Actor(image_id, x, y, dir, size, depth, true, world) {
    setVisible(true);
}

Actor::ENT_CLASS Item::getClass() {
    return ENT_ITEM;
}

/*************************************//**
*   OIL CLASS
*****************************************/

Oil::Oil(int x, int y, StudentWorld* world) : Item(IID_BARREL, x, y, right, 1.0, 2, world) {
    setVisible(false);
}

void Oil::doSomething() {
    if (!isAlive()) {
        return;
    }

    if (!isVisible() && getWorld()->inRadius(getX(), getY(), 4, {ENT_PLAYER})) {
        setVisible(true);
    }
    else if (isVisible() && getWorld()->inRadius(getX(), getY(), 3, {ENT_PLAYER})) {
        getWorld()->removeOil();
        getWorld()->increaseScore(1000);
        getWorld()->playSound(SOUND_FOUND_OIL);
        die();
    }
}

/*************************************//**
*   GOLD CLASS
*****************************************/

Gold::Gold(int x, int y, StudentWorld* world) : Item(IID_GOLD, x, y, right, 1.0, 2, world) {
    setVisible(false);
    state_ = PERM;
    lifetime_ = 100;
}

Gold::Gold(int x, int y, StudentWorld* world, STATE state) : Item(IID_GOLD, x, y, right, 1.0, 2, world) {
    setVisible(true);
    state_ = state;
    lifetime_ = 100;
}

Gold::STATE Gold::getState() {
    return state_;
}

void Gold::setState(STATE state) {
    state_ = state;
}

void Gold::doSomething() {
    if (!isAlive()) {
        return;
    }

    switch (getState()) {
        case PERM: {
            if (!isVisible() && getWorld()->inRadius(getX(), getY(), 4, {ENT_PLAYER})) {
                setVisible(true);
            }
            else if (isVisible() && getWorld()->inRadius(getX(), getY(), 3, {ENT_PLAYER})) {
                getWorld()->getIceman()->setGold(getWorld()->getIceman()->getGold() + 1);
                getWorld()->increaseScore(10);
                getWorld()->playSound(SOUND_GOT_GOODIE);
                die();
            }
            break;
        }
        case TEMP: {
            if (lifetime_ == 0) {
                die();
                return;;
            }

            auto protester = getWorld()->inRadius(getX(), getY(), 3, {ENT_NPC});
            if (isVisible() && protester) {
                protester->annoy(20, ENT_ITEM);
                die();
            }

            lifetime_--;
            break;
        }
    }
}

/*************************************//**
*   SONAR CLASS
*****************************************/

Sonar::Sonar(int x, int y, StudentWorld* world) : Item(IID_SONAR, x, y, right, 1.0, 2, world) {
    setVisible(true);
    state_ = TEMP;
    lifetime_ = (size_t) std::max(100, (int) (300 - 10 * getWorld()->getLevel()));
}

Sonar::STATE Sonar::getState() {
    return state_;
}

void Sonar::setState(STATE state) {
    state_ = state;
}

void Sonar::doSomething() {
    if (!isAlive()) {
        return;
    }

    switch (getState()) {
        case TEMP: {
            if (lifetime_ == 0) {
                die();
                return;
            }

            if (getWorld()->inRadius(getX(), getY(), 3, {ENT_PLAYER})) {
                getWorld()->getIceman()->setSonar(getWorld()->getIceman()->getSonar() + 1);
                getWorld()->increaseScore(75);
                getWorld()->playSound(SOUND_GOT_GOODIE);
                die();
            }

            lifetime_--;
            break;
        }
        default:
            break;
    }
}

/*************************************//**
*   WATER CLASS
*****************************************/

Water::Water(int x, int y, StudentWorld* world) : Item(IID_WATER_POOL, x, y, right, 1.0, 2, world) {
    setVisible(true);
    state_ = TEMP;
    lifetime_ = (size_t) std::max(100, (int) (300 - 10 * getWorld()->getLevel()));
}

Water::STATE Water::getState() {
    return state_;
}

void Water::setState(STATE state) {
    state_ = state;
}

void Water::doSomething() {
    if (!isAlive()) {
        return;
    }

    switch (getState()) {
        case TEMP: {
            if (lifetime_ == 0) {
                die();
                return;
            }

            if (getWorld()->inRadius(getX(), getY(), 3, {ENT_PLAYER})) {
                getWorld()->getIceman()->setWater(getWorld()->getIceman()->getWater() + 5);
                getWorld()->increaseScore(100);
                getWorld()->playSound(SOUND_GOT_GOODIE);
                die();
            }

            lifetime_--;
            break;
        }
        default:
            break;
    }
}

/*************************************//**
*   CHARACTER CLASS
*****************************************/

Character::Character(int image_id, int start_x, int start_y, GraphObject::Direction dir, size_t health,
                     StudentWorld* world) : Actor(image_id, start_x, start_y, dir, 1.0, 0, true, world) {
    this->health_ = health;
    setVisible(true);
}

/**
 * Get the Character's health.
 *
 * @return size_t Character health
 */
size_t Character::getHealth() const {
    return this->health_;
}

void Character::hurt(size_t damage) {
    this->health_ = (damage >= health_) ? 0 : health_ - damage;
    if (this->health_ < 1) {
        this->die();
    }
}

void Character::die() {
    Actor::die();
}

void Character::setHealth(size_t health) {
    health_ = (health < 0) ? 0 : health;
}

/*************************************//**
*   PROTESTER CLASS
*****************************************/

Protester::Protester(StudentWorld* world, int image_id, size_t health) : Character(image_id, 60, 60, left, health,
                                                                                   world) {
    state_ = NORMAL;
    last_state_ = NORMAL;
    shout_time_ = 0;
    turn_time_ = 0;
    tick_ = (size_t) std::max(0, (int) (3 - getWorld()->getLevel() / 4));
    amount_to_move_ = static_cast<size_t>(rand() % (60 + 1 - 8) + 8);
}

Actor::ENT_CLASS Protester::getClass() {
    return ENT_NPC;
}

Protester::STATE Protester::getState() {
    return state_;
}

Protester::STATE Protester::getLastState() {
    return last_state_;
}

size_t Protester::getScore() {
    return 100;
}

std::stack<std::pair<int, int>> &Protester::getPath() {
    return path_;
};

void Protester::setState(STATE state) {
    state_ = state;
}

void Protester::setLastState(Protester::STATE state) {
    last_state_ = state;
}

void Protester::setMove(size_t amount) {
    amount_to_move_ = amount;
}

void Protester::setTime(size_t time) {
    tick_ = time;
}

void Protester::setPath(const std::stack<std::pair<int, int>> &path) {
    path_ = path;
}

void Protester::doSomething() {
    if (!isAlive()) {
        return;
    }

    switch (getState()) {
        case REST: {
            rest();
            break;
        }
        case LEAVE: {
            leave();
            break;
        }
        case NORMAL: {
            action();
            break;
        }
    }
}

void Protester::rest() {
    if (--tick_ == 0) {
        setState(last_state_);
        setLastState(REST);
        setTime((size_t) std::max(0, (int) (3 - getWorld()->getLevel() / 4)));
    }
}

void Protester::leave() {
    setLastState(LEAVE);
    if (!getPath().empty()) {
        auto next_step = static_cast<std::pair<int, int> &&>(getPath().top());
        setDirection(getWorld()->getDir({getX(), getY()}, {next_step.first, next_step.second}));
        getPath().pop();
        moveTo(next_step.first, next_step.second);
    }

    if (getX() == 60 && getY() == 60) {
        die();
    }
    setState(REST);
}

void Protester::action() {
    setLastState(NORMAL);
    if (shout())
        return;

    if (chase())
        return;

    resetOrTurn();

    if (getWorld()->isClear(Area({getX(), getY()}, getDirection(), 1), {ENT_SOLID, ENT_WORLD})) {
        moveDir(getDirection(), 1);
    }
    else {
        setMove(0);
    }

    setState(REST);
}

bool Protester::chase() {
    auto iceman = getWorld()->getIceman();

    if ((getX() - iceman->getX() == 0 || getY() - iceman->getY() == 0)) {
        if (!getWorld()->inRadius(getX(), getY(), 4, {ENT_PLAYER})) {
            auto dir = getWorld()->getDir({getX(), getY()}, {iceman->getX(), iceman->getY()});
            auto bound = Area({getX(), getY()}, dir,
                              static_cast<size_t>((getX() - iceman->getX() == 0) ? std::abs(getY() - iceman->getY())
                                                                                 : std::abs(getX() - iceman->getX())));
            if (getWorld()->isClear(bound, {ENT_SOLID, ENT_WORLD})) {
                setDirection(dir);
                moveDir(dir, 1);
                setMove(0);
                setState(REST);
                return true;
            }
        }
    }

    return false;
}

bool Protester::shout() {
    if (shout_time_ == 0) {
        auto iceman = getWorld()->inRadius(getX(), getY(), 4, {ENT_PLAYER});
        if (iceman && getWorld()->getDir({getX(), getY()}, {iceman->getX(), iceman->getY()})) {
            getWorld()->playSound(SOUND_PROTESTER_YELL);
            iceman->annoy(2, ENT_NPC);
            shout_time_ = 15;
            setState(REST);
            return true;
        }
    }

    shout_time_ = (shout_time_ == 0) ? shout_time_ : shout_time_ - 1;

    return false;
}

void Protester::resetOrTurn() {
    setMove((amount_to_move_ > 0) ? amount_to_move_ - 1 : 0);
    turn_time_ = (turn_time_ > 0) ? turn_time_ - 1 : 0;
    if (amount_to_move_ == 0) {
        GraphObject::Direction dir;
        do {
            dir = static_cast<GraphObject::Direction>(rand() % 4 + 1);
        } while (!getWorld()->isClear(Area({getX(), getY()}, dir, 1), {ENT_WORLD, ENT_SOLID}));
        setDirection(dir);
        setMove(static_cast<size_t>(rand() % (60 + 1 - 8) + 8));
    }
    else if (turn_time_ == 0) {
        auto cur_dir = static_cast<int>(getDirection());
        GraphObject::Direction dir;
        if (cur_dir < 3) { // up or down
            cur_dir = 2;
            dir = static_cast<GraphObject::Direction>(cur_dir + (rand() % 2 + 1));

            if (!getWorld()->isClear(Area({getX(), getY()}, dir, 1), {ENT_WORLD, ENT_SOLID})) {
                dir = (static_cast<int>(dir) == 3) ? left : right;
            }

            if (!getWorld()->isClear(Area({getX(), getY()}, dir, 1), {ENT_WORLD, ENT_SOLID})) {
                dir = getDirection();
            }
            else {
                turn_time_ = 200;
                setMove(static_cast<size_t>(rand() % (60 + 1 - 8) + 8));
            }
        }
        else { // left or right
            cur_dir = 3;
            dir = static_cast<GraphObject::Direction>(cur_dir - (rand() % 2 + 1));

            if (!getWorld()->isClear(Area({getX(), getY()}, dir, 1), {ENT_WORLD, ENT_SOLID})) {
                dir = (static_cast<int>(dir) == 1) ? down : up;
            }

            if (!getWorld()->isClear(Area({getX(), getY()}, dir, 1), {ENT_WORLD, ENT_SOLID})) {
                dir = getDirection();
            }
            else {
                turn_time_ = 200;
                setMove(static_cast<size_t>(rand() % (60 + 1 - 8) + 8));
            }
        }

        setDirection(dir);
    }
}

void Protester::bribed() {
    getWorld()->increaseScore(25);
    setState(LEAVE);
}

void Protester::annoy(size_t health, ENT_CLASS attacker) {
    if (getState() == LEAVE && getLastState() == LEAVE)
        return;

    setHealth((health > getHealth()) ? 0 : getHealth() - health);
    if (getHealth() > 0) {
        getWorld()->playSound(SOUND_PROTESTER_ANNOYED);
        setTime((size_t) std::max(50, (int) (100 - getWorld()->getLevel() * 10)));
    }
    else {
        if (attacker == ENT_PLAYER) {
            getWorld()->increaseScore(100);
            getWorld()->playSound(SOUND_PROTESTER_GIVE_UP);
        }
        else if (attacker == ENT_SOLID) {
            getWorld()->increaseScore(getScore());
            getWorld()->playSound(SOUND_PROTESTER_GIVE_UP);
        }
        else if (attacker == ENT_ITEM) {
            getWorld()->playSound(SOUND_PROTESTER_FOUND_GOLD);
            bribed();
        }

        auto path = getWorld()->getPath(std::make_pair(getX(), getY()), std::make_pair(COL - 4, COL - 4), 4);
        if (path.first) {
            setPath(path.second);
            setState(LEAVE);
        }
        else {
            getWorld()->debug("FAILED TO FIND PATH!\n");
        }
    }
}

void Protester::die() {
    Character::die();
}

/*************************************//**
*   HARDCORE PROTESTER CLASS
*****************************************/

HardcoreProtester::HardcoreProtester(StudentWorld* world, int image_id, size_t health) : Protester(world, image_id,
                                                                                                   health) {
}

size_t HardcoreProtester::getScore() {
    return 500;
}

void HardcoreProtester::action() {
    setLastState(NORMAL);
    if (shout())
        return;

    if (!chase())
        if (!cellphone())
            resetOrTurn();

    if (getWorld()->isClear(Area({getX(), getY()}, getDirection(), 1), {ENT_SOLID, ENT_WORLD})) {
        moveDir(getDirection(), 1);
    }
    else {
        setMove(0);
    }

    setState(REST);
}

void HardcoreProtester::bribed() {
    getWorld()->increaseScore(50);
    setTime((size_t) std::max(50, (int) (100 - getWorld()->getLevel() * 10)));
}

bool HardcoreProtester::cellphone() {

    auto M = 16 + getWorld()->getLevel() * 2;
    auto iceman = getWorld()->getIceman();
    auto path = getWorld()->getPath({getX(), getY()}, {iceman->getX(), iceman->getY()}, 4);

    if (path.first) {
        if (path.second.size() <= M) {
            path.second.pop();
            auto step = static_cast<std::pair<int, int> &&>(path.second.top());
            setDirection(getWorld()->getDir({getX(), getY()}, {step.first, step.second}));
            setState(REST);
            return true;
        }
    }

    return false;
}

/*************************************//**
*   ICEMAN CLASS
*****************************************/

Iceman::Iceman(StudentWorld* world, int image_id, int start_x, int start_y, GraphObject::Direction dir,
               size_t health) : Character(image_id, start_x, start_y, dir, health, world) {
    water_ = 5;
    sonar_ = 1;
    gold_ = 0;
    setVisible(true);
}

Actor::ENT_CLASS Iceman::getClass() {
    return ENT_PLAYER;
}

size_t Iceman::getWater() const {
    return water_;
}

size_t Iceman::getSonar() const {
    return sonar_;
}

size_t Iceman::getGold() const {
    return gold_;
}

void Iceman::setWater(size_t water) {
    water_ = water;
}

void Iceman::setSonar(size_t sonar) {
    sonar_ = sonar;
}

void Iceman::setGold(size_t gold) {
    gold_ = gold;
}

void Iceman::die() {
    getWorld()->playSound(SOUND_PLAYER_GIVE_UP);
    Character::die();
}

void Iceman::doSomething() {
    if (getHealth() <= 0) {
        die();
        return;
    }

    int key_pressed;
    if (getWorld()->getKey(key_pressed)) {
        switch (key_pressed) {
            case KEY_PRESS_ESCAPE:
                die();
                break;
            case KEY_PRESS_UP:
                if (getDirection() != up) {
                    setDirection(up);
                }
                else if (getY() + 1 <= ROW && !getWorld()->isSolid(getX(), getY() + 1, 3)) {
                    moveDir(up, 1);
                }
                else {
                    moveTo(getX(), getY());
                }
                break;
            case KEY_PRESS_LEFT:
                if (getDirection() != left) {
                    setDirection(left);
                }
                else if (getX() - 1 >= 0 && !getWorld()->isSolid(getX() - 1, getY(), 3)) {
                    moveDir(left, 1);
                }
                else {
                    moveTo(getX(), getY());
                }
                break;
            case KEY_PRESS_DOWN:
                if (getDirection() != down) {
                    setDirection(down);
                }
                else if (getY() - 1 >= 0 && !getWorld()->isSolid(getX(), getY() - 1, 3)) {
                    moveDir(down, 1);
                }
                else {
                    moveTo(getX(), getY());
                }
                break;
            case KEY_PRESS_RIGHT:
                if (getDirection() != right) {
                    setDirection(right);
                }
                else if (getX() + 1 <= ROW && !getWorld()->isSolid(getX() + 1, getY(), 3)) {
                    moveDir(right, 1);
                }
                else {
                    moveTo(getX(), getY());
                }
                break;
            case KEY_PRESS_SPACE:
                squirt();
                break;
            case KEY_PRESS_TAB:
                drop();
                break;
            case 'Z':
            case 'z':
                sonar();
                break;
#ifdef ICEMAN_DEBUG
            case 'P':
            case 'p': {
                getWorld()->debug("[PATH TEST] " + std::to_string(getX()) + ", " + std::to_string(getY()));
                getWorld()->debug("[PATH TEST] Getting path...\n");
                auto path = getWorld()->getPath({getX(), getY()}, {60, 60}, 4);
                if (path.first) {
                    while (!path.second.empty()) {
                        auto cur = static_cast<std::pair<int, int> &&>(path.second.top());
                        path.second.pop();
                      getWorld()->debug(
                               "[PATH] NEXT STEP (" + std::to_string(cur.first) + ", " + std::to_string(cur.second) +
                              ")\n");

                        moveTo(cur.first, cur.second);
                    }
                }
                else {
                    getWorld()->debug("[PATH TEST] Pathing failed!\n");
                }
            }
#endif
            default:
                break;
        }
    }
}

void Iceman::annoy(size_t health, ENT_CLASS attacker) {
    hurt(health);
}

void Iceman::squirt() {
    if (!getWater()) {
        return;
    }

    Direction dir = getDirection();
    Area vec = Area(std::make_pair(getX(), getY()), dir, 4);
    if (getWorld()->isClear(vec, {ENT_WORLD, ENT_SOLID})) {
        getWorld()->playSound(SOUND_PLAYER_SQUIRT);
        setWater(getWater() - 1);
        getWorld()->spawnObject(std::make_shared<Squirt>(vec.tail.first, vec.tail.second, dir, getWorld()));
    }
}

void Iceman::drop() {
    if (!getGold())
        return;

    setGold(getGold() - 1);

    getWorld()->spawnObject(std::make_shared<Gold>(getX(), getY(), getWorld(), Gold::STATE::TEMP));
}

void Iceman::sonar() {
    if (getSonar() <= 0)
        return;
    setSonar(getSonar() - 1);
    getWorld()->playSound(SOUND_SONAR);
    getWorld()->revealObjects({getX(), getY()}, 12);
}

/*************************************//**
*   AREA CLASS
*****************************************/

Area::Area(std::pair<int, int> pair, GraphObject::Direction dir, size_t size)
        : tail(pair), head(pair), dir(dir), size(size) {
    switch (dir) {
        case GraphObject::up:
            tail = std::make_pair(tail.first, tail.second + 4);
            head = std::make_pair(tail.first + 4, tail.second + size);
            break;
        case GraphObject::down:
            tail = std::make_pair(tail.first, tail.second - size);
            head = std::make_pair(tail.first + 4, tail.second + size);
            break;
        case GraphObject::left:
            tail = std::make_pair(tail.first - size, tail.second);
            head = std::make_pair(tail.first + size, tail.second + 4);
            break;
        case GraphObject::right:
            tail = std::make_pair(tail.first + 4, tail.second);
            head = std::make_pair(tail.first + size, tail.second + 4);
            break;
        default:
            tail = std::make_pair(tail.first, tail.second);
            head = std::make_pair(tail.first + 4, tail.second + 4);
    }
}
